from intro.forms import Schedule
from intro.forms import JadwalForm

def susunSchedule(request):
    if request.method == 'POST':
        form = Schedule(request.POST)
        if form.is_valid():
            hari = form.cleaned_data['hari']
            nama_kegiatan = form.cleaned_data['nama_kegiatan']
            tempat = form.cleaned_data['tempat']
            kategori = form.cleaned_data['kategori']

    else:
        form = Schedule()
    return render(request, "forms.html", {'form': form})

def jadwal_detail(request):
     if request.method == 'POST':
        form = JadwalForm(request.POST)
        if form.is_valid():
            print("tentu")
            form.save()
     form = JadwalForm()
     return render(request, "forms.html", {'form': form})