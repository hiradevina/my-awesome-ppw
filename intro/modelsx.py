from django.db import models

# Create your models here.

class Jadwal(models.Model):
    hari = models.DateTimeField()
    nama_kegiatan = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)

    def __str__(self):
        return self.tempat
