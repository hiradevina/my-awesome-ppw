# from crispy_forms.helper import FormHelper
# from crispy_forms.layout import Layout, Submit

from django import forms
from .models import Jadwal
class Schedule(forms.Form):
    nama_kegiatan = forms.CharField(label='Nama Kegiatan')
    hari = forms.CharField(label="Hari")
    tanggal = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
    waktu = forms.TimeField(widget=forms.TimeInput(attrs={'type': 'time'}))
    tempat = forms.CharField(label="tempat")
    kategori = forms.CharField(label = "Kategori")


