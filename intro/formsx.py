from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit

from django import forms
from .models import Jadwal
class Schedule(forms.Form):

    hari = forms.DateTimeField(required=True, label="Date and Time Example: 04/30/2018 14:30")
    nama_kegiatan = forms.CharField(strip=True, required=True)
    tempat = forms.CharField(strip=True, required=True)
    kategori = forms.CharField(strip=True, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_method = 'post'

        self.helper.layout = Layout(
            'hari',
            'nama_kegiatan',
            'tempat',
            'kategori',
            Submit('submit', 'Submit', css_class='btn-success')
        )
class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ('hari', 'nama_kegiatan', 'tempat', 'kategori')
        # widgets = {
        #     'hari': forms.DateInput(attrs={'class':'datepicker'}),
        # }
