from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Jadwal
from intro.forms import Schedule
# from intro.forms import JadwalForm

def homepage(request):
    return render(request, "home.html", {})
def about(request):
    return render(request, "index.html", {})
def msg(request):
    return render(request, "form.html", {})
def desc(request):
    return render(request, "description.html", {})

def allSchedule(request):
    schedules = Jadwal.objects.all()
    return render(request, 'form_result.html', {'schedules':schedules})


def deleteSchedule(request):
    schedules = Jadwal.objects.all().delete()
    return render(request, 'form_result.html', {'schedules':schedules})

def formSchedule(request):
    form = Schedule()
    return render(request,'forms.html', {'form':form})

def susunSchedule(request):
    form = Schedule(request.POST)
    if form.is_valid():
        hari = request.POST.get("hari")
        tanggal = request.POST.get("tanggal")
        waktu = request.POST.get("waktu")
        nama_kegiatan = request.POST.get("nama_kegiatan")
        tempat = request.POST.get("tempat")
        kategori = request.POST.get("kategori")
        Jadwal.objects.create(nama_kegiatan=nama_kegiatan, hari=hari, waktu=waktu, tanggal=tanggal, tempat=tempat, kategori=kategori)
        # schedules = Jadwal.objects.all()
        # return render(request, 'form_result.html', {'schedules':schedules})
        return redirect('intro:allSchedule')
    else:
        return render(request, 'forms.html', {})