from django.db import models

# Create your models here.
class Jadwal(models.Model):
    nama_kegiatan = models.CharField(max_length=200)
    hari = models.CharField(max_length=20)
    tanggal = models.DateField()
    waktu = models.TimeField()
    tempat = models.CharField(max_length=200)
    kategori = models.CharField(max_length=200)