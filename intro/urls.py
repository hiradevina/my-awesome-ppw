from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static

app_name = 'intro'

urlpatterns = [
    path('', views.homepage, name="home"),
    path('about/', views.about, name="about"),
    path('contact', views.msg, name="contact"),
    path('desc', views.desc, name="desc"),
    path('susunjadwal', views.formSchedule, name="susunJadwal"),
    path('submitSchedule', views.susunSchedule, name="submitSchedule"),
    path('allSchedule',views.allSchedule, name="allSchedule"),
    path('deleteSchedule', views.deleteSchedule, name="deleteSchedule"),
    # path('detail', views.jadwal_detail),
] 
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)